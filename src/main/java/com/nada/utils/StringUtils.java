package com.nada.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class StringUtils
{
  public static final String EMPTY = "";

  /**
   * @param toJoin
   * @param delimiter
   * @return
   */
  public static <T> String join(Iterable<T> toJoin, String delimiter)
  {
    Iterator<?> oIter;
    if (toJoin == null || (!(oIter = toJoin.iterator()).hasNext())) {
      return "";
    }
    delimiter = delimiter == null ? EMPTY : delimiter;
    StringBuilder oBuilder = new StringBuilder(String.valueOf(oIter.next()));
    while (oIter.hasNext()) {
      oBuilder.append(delimiter).append(oIter.next());
    }
    return oBuilder.toString();
  }


  /**
   * @param toJoin
   * @param delimiter
   * @return
   */
  public static <T> String join(final T[] toJoin, String delimiter)
  {
    return join(new Iterable<T>()
    {
      @Override
      public Iterator<T> iterator()
      {
        return new Iterator<T>()
        {
          private int currIdx = 0;

          @Override
          public boolean hasNext()
          {
            return toJoin == null ? false : currIdx < toJoin.length;
          }

          @Override
          public T next()
          {
            if (toJoin == null || currIdx == toJoin.length) {
              throw new NoSuchElementException("current index: " + currIdx);
            }
            return toJoin[currIdx++];
          }

          @Override
          public void remove()
          {
            // no-op -- NOT USED.
          }
        };
      }
    }, delimiter);
  }

}
